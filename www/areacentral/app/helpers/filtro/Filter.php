<?php

class Filter {
    
  const OPERATOR_MAP = [
      'LIKE' => ['template' => '%1$s LIKE :%1$s:', 'value' => '%%%s%%']
    , '='    => ['template' => '%1$s = :%1$s:'   , 'value' => '%s']
  ];
  
  private $field;
  private $modelField;
  private $operator;
  private $value;

  /**
   * Get the value of field
   */ 
  public function getField()
  {
    return $this->field;
  }

  /**
   * Set the value of field
   *
   * @return  self
   */ 
  public function setField($field)
  {
    $this->field = $field;

    return $this;
  }
    
   /**
   * Set the value of operator
   *
   * @return  self
   */ 
  public function setOperator($operator)
  {
    $this->operator = $operator;

    return $this;
  }
    

  /**
   * Get the value of operator
   */ 
  public function getOperator()
  {
    return $this->operator;
  }
  

  /** 
   * Get Label Operator
   */
  public function getLabelOperator() {
    switch($this->getOperator()) {
      case 'LIKE':
        return 'contendo';
        break;
      case '=':
        return 'igual';
        break;
    }
  }

  /**
   * Get Tag Operator
  */
  public function getTagOperator() 
  {
    switch($this->getOperator()) {
      case 'LIKE':
        return "%1\$s LIKE :%1\$s:";
        break;
      case '=':
        return "%s = '%s'";
        break;
    }
  }  
  
  /**
   * Get the value of value
   */ 
  public function getValue()
  {
    return $this->value;
  }

  /**
   * Set the value of value
   *
   * @return  self
   */ 
  public function setValue($value)
  {
    $this->value = $value;

    return $this;
  }

  /**
   * Get the value of modelField
   */ 
  public function getModelField()
  {
    return $this->modelField;
  }

  /**
   * Set the value of modelField
   *
   * @return  self
   */ 
  public function setModelField($modelField)
  {
    $this->modelField = $modelField;

    return $this;
  }

  /**
   * Gera clausula para find do model
   */
  public function getClause()
  {

    $aBind = [];
    $aBind[$this->getModelField()] = sprintf(self::OPERATOR_MAP[$this->getOperator()]['value'], $this->getValue());

    return [
        'condition' => sprintf(self::OPERATOR_MAP[$this->getOperator()]['template'], $this->getModelField())
      , 'bind'      => $aBind
    ];
  }
}