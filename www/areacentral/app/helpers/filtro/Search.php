<?php

/**
 * Classe responsavel por gerenciar filtros
 */

class Search {

  /**
   * Adiciona filtro a sessão 
   * 
   * @param string $sField - Campo a ser filtrado
   * @param string $sOperator - Operador a ser utilizado
   * @param string $sValue - Valor para filtrar
   * 
   * @return void
   */
  public static function add($sField, $sModelField, $sOperator, $sValue) 
  {    
    global $di;        
    $oSession = $di->getSession();
    $sControler = $di->getRouter()->getControllerName();
    $sAction = $di->getRouter()->getActionName();

    $oFilter = new Filter();
    $oFilter->setField($sField);        
    $oFilter->setModelField($sModelField);
    $oFilter->setOperator($sOperator);
    $oFilter->setValue($sValue);

    //Filtro Atual    
    $aSession = $oSession->get('filter') ?: [];
    //Incrementa
    $aSession[$sControler][$sAction][] = serialize($oFilter);    
    //Salva    
    $oSession->set('filter', $aSession);
  }
  
  /**
   * Gera clausulas de filtro
   *    
   * @return array Clausulas
   */
  public static function getClauses() {
    global $di;
    $oSession = $di->getSession();
    $sControler = $di->getRouter()->getControllerName();
    $sAction = $di->getRouter()->getActionName();
    
    //Filtro Atual    
    $aSession = $oSession->get('filter') ?: [];

    $aSession = $aSession[$sControler][$sAction];
    
    $aClauses = [
        'conditions' => []
      , 'bind'       => []
    ];

    foreach ($aSession as $aFilter) {
      $oFilter = unserialize($aFilter);      
      $aFilterClause = $oFilter->getClause();
      
      $aClauses['conditions'][] = $aFilterClause['condition'];
      $aClauses['bind'] = array_merge($aClauses['bind'], $aFilterClause['bind']); 
    }

    $aReturn = [
      implode(' AND ', $aClauses['conditions'])
      , 'bind' => $aClauses['bind']
    ];

    return $aReturn;

  }  

  public static function getViewFilters($aFilterMap) {
    global $di;
    $oSession = $di->getSession();
    $sControler = $di->getRouter()->getControllerName();
    $sAction = $di->getRouter()->getActionName();
    
    //Filtro Atual    
    $aSession = $oSession->get('filter') ?: [];
    $aSession = $aSession[$sControler][$sAction];
    
    $aReturn = [];
    foreach ($aSession as $iKey => $aFilter) {
      $oFilter = unserialize($aFilter);    
      
      $aReturn[] = (object) [
          'key'      => $iKey
        , 'field'    => $oFilter->getField()
        , 'operator' => $oFilter->getLabelOperator()
        , 'value'    => $oFilter->getValue()
        , 'label'    => $aFilterMap[$oFilter->getField()]['label']
      ];
    }

    return $aReturn;
  }

  public static function removeFilter($iKey) {
    global $di;
    $oSession = $di->getSession();
    $sControler = $di->getRouter()->getControllerName();
    $sAction = $di->getRouter()->getActionName();
    
    //Filtro Atual    
    $aSession = $oSession->get('filter') ?: [];
    unset($aSession[$sControler][$sAction][$iKey]);
    $oSession->set('filter', $aSession);

  }
}