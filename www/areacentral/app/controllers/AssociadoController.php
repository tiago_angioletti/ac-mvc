<?php
use Phalcon\Mvc\View;
use Phalcon\Config;
use Phalcon\Http\Response;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class AssociadoController extends \Phalcon\Mvc\Controller
{  

  //Mapeamento de filtros
  const FILTER_MAP = [
      'nome'     => ['modelField' => 'NOME'        , 'operator' => 'LIKE', 'label' => 'Nome Fantasia']
    , 'cnpj'     => ['modelField' => 'CNPJ'        , 'operator' => 'LIKE', 'label' => 'CNPJ']
    , 'codigo'   => ['modelField' => 'CODIGO'      , 'operator' => '='   , 'label' => 'Código']
    , 'razao'    => ['modelField' => 'RAZAOSOCIAL' , 'operator' => 'LIKE', 'label' => 'Razão Social']
    , 'telefone' => ['modelField' => 'TELEFONE'    , 'operator' => 'LIKE', 'label' => 'Telefone']
    , 'uf'       => ['modelField' => 'UF'          , 'operator' => 'LIKE', 'label' => 'UF']
  ];

  public function listaAction() { 
    
    //Inicio de variaveis da view
    $this->view->filtros = [];
    $this->view->associados = [];
    $this->view->urlEstilo  = sprintf(
      '%sempresas/%s/estilo.css'
      , $this->config->application->urlAc
      , $this->config->application->empresa
    );

    $this->view->urlDetalhe = sprintf(
      '%?pg=%s_associado_detalhe&associado='
      , $this->config->aplication->urlAC
      , $this->request->getQuery('area', 'string')
    );
    
    //Filtros
    $this->listaSearch();    

    //Dados
    $iCurrentPage = (int) $this->request->getQuery('page', 'int');
    $oPaginator = new PaginatorModel([
        'data'  => Associado::find(Search::getClauses())
      , 'limit' => 20
      , 'page'  => $iCurrentPage
    ]);

    $this->view->page = $oPaginator->getPaginate();
    for ($i = 1; $i <= $this->view->page->total_pages; $i++) {
      $this->view->page->pages[] = $i;
    }
    
    //Caso mobile
    if ($this->request->isAjax() && $this->config->application->isMobile) {            
      $this->view->disableLevel(
        View::LEVEL_MAIN_LAYOUT
      );
    }
  }

  public function detalheAction($iCodigoAssociado){      
    $this->view->associado = Associado::findByCodigo($iCodigoAssociado);
    $this->view->matriz    = Associado::findByCodigo($this->view->associado->EMP_CODIGO);

    if ($this->request->isAjax() && $this->config->application->isMobile) {
      $this->view->disableLevel(
        View::LEVEL_MAIN_LAYOUT
      );
    }
  }

  private function listaSearch() {
    //Tratamento de Filtros
    if ($this->request->isPost() && !empty($this->request->getPost('field'))) {

      Search::add(
          $this->request->getPost('field')
        , self::FILTER_MAP[$this->request->getPost('field')]['modelField']
        , self::FILTER_MAP[$this->request->getPost('field')]['operator']
        , $this->request->getPost('value')
      );

      $this->response->redirect('lista/', true, 302);
      return $this->response;
      
    }
    
    //Remove filtro
    Search::removeFilter($this->request->get('remove'));
        
    $this->view->filtros = Search::getViewFilters(self::FILTER_MAP);
  }
}
