<?php
use Phalcon\Mvc\View;
use Phalcon\Config;
use Phalcon\Http\Response;
#use Helpers\Filtro;

class IndexController extends \Phalcon\Mvc\Controller
{  

  public function indexAction() { 
    $this->response->redirect('associado/lista/', true, 302);
    return $this->response;
  }
}