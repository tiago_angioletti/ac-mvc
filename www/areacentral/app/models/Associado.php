<?php

class Associado extends Empresa
{
    public static function find($aParams = [])
    {                               
        $aDefaultModelParams = [
              'condition' => 'TIPO IN(:TIPO1:,:TIPO2:,:TIPO3:)'
            , 'bind'  => [
                'TIPO1' => 'S'
              , 'TIPO2' => 'A'
              , 'TIPO3' => 'C'
            ]
        ];        
        
        $sCondition = sprintf(
            '%s %s %s'
            , $aDefaultModelParams['condition']
            , !empty($aParams[0]) ? 'AND' : ''
            , $aParams[0]
        );

        $aParams = [
            $sCondition
            , 'bind'   => array_merge($aDefaultModelParams['bind'], $aParams['bind'] ?: [])
            , ['order' => 'NOME']
        ];                    
        return parent::find($aParams);        
    }
    
}