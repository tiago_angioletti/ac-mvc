<?php

class Empresa extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $CODIGO;

    /**
     *
     * @var string
     */
    public $NOME;

    /**
     *
     * @var string
     */
    public $RAZAOSOCIAL;

    /**
     *
     * @var string
     */
    public $CNPJ;

    /**
     *
     * @var string
     */
    public $TIPOPESSOA;

    /**
     *
     * @var string
     */
    public $IE;

    /**
     *
     * @var string
     */
    public $REFERENCIA;

    /**
     *
     * @var string
     */
    public $DATAFUNDACAO;

    /**
     *
     * @var string
     */
    public $ENDERECO;

    /**
     *
     * @var string
     */
    public $LOGRADOURO;

    /**
     *
     * @var string
     */
    public $COMPLEMENTO;

    /**
     *
     * @var string
     */
    public $NUMERO;

    /**
     *
     * @var string
     */
    public $BAIRRO;

    /**
     *
     * @var string
     */
    public $CEP;

    /**
     *
     * @var string
     */
    public $CIDADE;

    /**
     *
     * @var string
     */
    public $UF;

    /**
     *
     * @var string
     */
    public $VENDEDOR;

    /**
     *
     * @var string
     */
    public $TELEFONE;

    /**
     *
     * @var string
     */
    public $EMAIL;

    /**
     *
     * @var string
     */
    public $EMAILFINANCEIRO;

    /**
     *
     * @var string
     */
    public $REGIME;

    /**
     *
     * @var string
     */
    public $HOMEPAGE;

    /**
     *
     * @var string
     */
    public $COMUNICADORTIPO;

    /**
     *
     * @var string
     */
    public $COMUNICADOR;

    /**
     *
     * @var string
     */
    public $CANCO;

    /**
     *
     * @var string
     */
    public $TIPO;

    /**
     *
     * @var string
     */
    public $ATIVO;

    /**
     *
     * @var string
     */
    public $OBSERVACAO;

    /**
     *
     * @var integer
     */
    public $EMP_CODIGO;

    /**
     *
     * @var integer
     */
    public $REGIAO;

    /**
     *
     * @var integer
     */
    public $ORDEM;

    /**
     *
     * @var string
     */
    public $DATACADASTRO;

    /**
     *
     * @var string
     */
    public $DATAALTERACAO;

    /**
     *
     * @var string
     */
    public $DATAEXCLUSAO;

    /**
     *switch()
     * @var string
     */
    public $USU_APELIDO;

    /**
     *
     * @var string
     */
    public $USU_ALTERACAO;

    /**
     *
     * @var string
     */
    public $USU_EXCLUSAO;

    /**
     *
     * @var string
     */
    public $BONVIGENCIA;

    /**
     *
     * @var double
     */
    public $BONPERCENTUAL;

    /**
     *
     * @var string
     */
    public $BONFORMAPAGAMENTO;

    /**
     *
     * @var string
     */
    public $BONLANCAMENTOAVULSO;

    /**
     *
     * @var string
     */
    public $BONNEGOCIADO;

    /**
     *
     * @var doubleswitch()
     */
    public $PEDIDOMINIMO;

    /**
     *
     * @var string
     */
    public $DATAADESAO;

    /**
     *
     * @var string
     */
    public $DATAADEQUACAO;

    /**
     *
     * @var string
     */
    public $DATADESLIGAMENTO;

    /**
     *
     * @var string
     */
    public $BASE;

    /**
     *
     * @var string
     */
    public $IMAGEM;

    /**
     *
     * @var string
     */
    public $CERTNUMEROSERIE;

    /**
     *
     * @var string
     */
    public $CERTDATAINSTALACAO;

    /**
     *
     * @var string
     */
    public $CERTVENCIMENTO;

    /**
     *
     * @var string
     */
    public $CERTULTIMACONSULTA;

    /**
     *
     * @var integer
     */
    public $CERTULTIMONSU;

    /**
     *
     * @var integer
     */
    public $cERTMAXNSU;

    /**
     *
     * @var string
     */
    public $cERTUSULOGIN;

    /**
     *
     * @var string
     */
    public $cERTDATALOGIN;

    /**
     *
     * @var string
     */
    public $cERTIPLOGIN;

    /**
     *
     * @var string
     */
    public $cERTAPPVERSAO;

    /**
     *
     * @var string
     */
    public $cERTOBSERVACAO;

    /**
     *
     * @var string
     */
    public $cERTERRO;

    /**
     *
     * @var string
     */
    public $CERTATIVO;

    /**
     *
     * @var string
     */
    public $CERTMANIFESTAR;

    /**
     *
     * @var string
     */
    public $CERTINSTALAR;

    /**
     *
     * @var string
     */
    public $CERTTIPO;

    /**
     *
     * @var double
     */
    public $PERCENTUALMETAS;

    /**
     *
     * @var string
     */
    public $REDE;

    /**
     *
     * @var double
     */
    public $PERCENTUALCOMPRAS;

    /**
     *
     * @var double
     */
    public $CAPITALSOCIAL;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource('EMPRESA');
        
        $this->hasMany(
            'CODIGO' //Codigo da empresa
          , 'Usuario' //No Model Usuario
          , 'EMP_CODIGO' // é o campo emp_codigo
        );
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'EMPRESA';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Empresa[]|Empresa|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Empresa|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /** 
     * Busca unica por codigo de empresa
     * 
     * @param int $iCodigo - Codigo da empresa
     * @return Empresa|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findByCodigo($iCodigo) 
    {
        $aFiltro = [
              'CODIGO = :CODIGO:'
            , 'bind' => [
                'CODIGO' => $iCodigo
            ]
        ];
        return parent::findFirst($aFiltro);
    }
      
}
