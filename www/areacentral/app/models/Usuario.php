<?php

class Usuario extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $CODIGO;

    /**
     *
     * @var string
     */
    public $NOME;

    /**
     *
     * @var string
     */
    public $DATANASCIMENTO;

    /**
     *
     * @var string
     */
    public $EMAIL;    

    /**
     *
     * @var string
     */
    public $TELEFONE;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource('USUARIO');
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'USUARIO';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Usuario[]|Usuario|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Usuario|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
