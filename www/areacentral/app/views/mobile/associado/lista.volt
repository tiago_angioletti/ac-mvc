

<div class="actionbar actionbar-mobile">
    <div class="actionbar-action" id="menu-collapse">
        <img src="../../../../areacentral/public/img/menu.svg" alt="">
    </div> 

    <div class="actionbar-content">
        <div class="title-page">
            Associados
        </div>
    </div>
</div>
<div class="actionbar-wrapper"></div>

<main class="container">
    <div class="card list-items">
        {% for associado in page.items %}   
            <a href="{{ '/areacentral/associado/detalhe/' ~ associado.CODIGO }}">
                <div class="item-block">                
                    <div class="text-h600">
                        {{ associado.NOME }}
                    </div>
        
                    <div class="text-p">
                        {{ associado.RAZAOSOCIAL }}
                    </div>
                </div>            
            </a>            
        {% endfor %}
    </div> 
</main>  

<footer>
    Meu rodapé
</footer>