<div class="actionbar actionbar-mobile actionbar-light">
    <div class="actionbar-action return">
        <img src="/areacentral/public/img/arrow-left-dark.svg"> 
    </div> 

    <div class="actionbar-content">
        <div class="title-page">
          Detalhe do Associado
        </div>
    </div>    
  </div>

<div class="container">
  <div class="p-t-25 p-b-25">
      <label class="text-p">Nome fantasia</label>
      <div class="text-h600">
        {{ associado.NOME }}
      </div>
        
      <label class="text-p">Razão social</label>
      <div class="text-h600">
        {{ associado.RAZAOSOCIAL }}
      </div>
            
      <label class="text-p">Telefone</label>
      <div class="text-h600">
        {{ associado.TELEFONE }}
      </div>
  </div>
  
  <div class="area-legend">USUÁRIOS DESTE ASSOCIADO</div>
</div>
