<link rel="stylesheet" href="{{ urlEstilo }}">
<div class="card">
    <form action="{{url('associado/lista')}}" method="post">                
        <div class="card-header">
            <h2>Filtrar</h2>
        </div>
        <div class="card-body form-inline">
            <div class="card-row">
                <div class="form-group">
                    <select class="form-control" name="field" id="filtro_opcoes" title="Coluna para Filtrar">
                        <option value="nome">Nome Fantasia</option>
                        <option value="cnpj">CNPJ</option>
                        <option value="codigo">Código</option>
                        <option value="razao">Razão Social</option>
                        <option value="telefone">Telefone</option>
                        <!--option value="marcador">Marcador (Associado)</option-->
                        <!--option value="usuario">Usuário</option-->
                        <option value="uf">UF</option>
                    </select>
                </div>

                <div class="form-group">
                    <input class="form-control" name="value" id="filtro_valor" title="Valor para Filtrar" type="text" size="30" value="">
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-success" value="Filtrar">
                </div>               
            </div>

            <div class="card-row m-t-10">
                    {% for filtro in filtros %}
                    <a href="?remove={{filtro.key}}" title="Remover filtro" class="btn btn-default btn-xs items-filtered">
                    <span class="pull-left">
                    <strong>{{filtro.label}}</strong>
                    {{filtro.operator}}
                    &nbsp;
                    </span>
                    <span style="text-decoration:underline">{{filtro.value}}</span>&nbsp;&nbsp;
                    <i class="fa fa-times-circle" aria-hidden="true"></i>
                    {% endfor %}              
                </a>
            </div>
        </div>
    </form>
</div>
<div class="card">
    <div class="card-header">
        <h2>Lista de associados</h2>
    </div>
    <div class="card-body form-inline">
        <table class="grid">
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>Cód</th>
                    <th>CNPJ</th>
                    <th>Nome Fantasia</th>
                    <th>Razão Social</th>
                    <th>Telefone(s)</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                {% for associado in page.items %}
                <tr>
                    <td class="centralizado">
                        <input name="selecionar_grupo[]" type="checkbox">
                    </td>
                    <td>{{ associado.CODIGO }}</td>
                    <td>{{ associado.CNPJ }}</td>
                    <td><a href="{{ urlDetalhe }}{{ associado.CODIGO }}" target="_top">{{associado.NOME }}</a></td>
                    <td>{{ associado.RAZAOSOCIAL }}</td>
                    <td>{{ associado.TELEFONE }}</td>
                    <td width="4%">
                        <span class="dropdown" title="Opções">Opções</span>
                        <ul class="menu-dropdown">
                            <li>
                                <a href="?pg=administrador_mensagem_nova&amp;associado=7" title="Enviar mensagem">
                                    <img src="imagens/icones/mail.png">
                                    Enviar mensagem
                                </a>
                            </li>
                            <li>
                                <a href="index_detalhe.php?pg=administrador_agenda_nova&amp;associado=7&amp;TB_iframe=true&amp;width=980&amp;height=500" class="" title="Agendar compromisso"><img src="imagens/icones/calendar.png">Agendar compromisso</a></li> <li><a href="?pg=administrador_associado_alterar&amp;associado=7" title="Alterar associado"><img src="imagens/icones/pencil.png">Alterar</a></li> <li><a rel="?pg=administrador_associados_lixeira" href="?pg=administrador_associados&amp;acao=excluir_associado&amp;associado=7" title="Excluir associado"><i class="fa fa-trash-o color-red" aria-hidden="true"></i>Excluir</a></li> <li><a href="?pg=administrador_associado_analise_desempenho&amp;associado=7" title="Análise de desempenho do associado"><img src="imagens/icones/column-chart.png">Análise de desempenho</a></li> <li><a rel="?pg=administrador_ocorrencias" href="?pg=administrador_associado_detalhe&amp;associado=7#ocorrencias" title="Ocorrências do associado"><img src="imagens/icones/document_warning.png">Ocorrências do associado</a></li> <li><a rel="?pg=administrador_usuarios" href="?pg=administrador_associado_detalhe&amp;associado=7#usuarios" title="Usuários do associado"><img src="imagens/icones/user1.png">Usuários do associado</a></li> <li><a class="" href="index_detalhe.php?pg=administrador_arquivo_novo&amp;modulo=associado&amp;associado=7&amp;TB_iframe=true&amp;height=520&amp;width=1170" title="Anexar arquivos"><img src="imagens/icones/clip.png">Anexar arquivos</a>
                            </li>
                        </ul>
                      </td>
                </tr>
                {% endfor %}
            </tbody>
        </table>
    </div>
    <div class="rodape">
        <div class="paginacao">
            <p>
                <a href="/areacentral/associado" title="Primeira Página">«</a>
                <a href="/areacentral/associado?page={{ page.before }}" title="Página Anterior">‹</a>
                {% for num_page in page.pages %}
                    {% if num_page === page.current %}
                        <span>{{ num_page }}</span>
                    {% else %}
                        <a href="/areacentral/associado?page={{ num_page }}">{{ num_page }}</a>
                    {% endif %}
                {% endfor %}
                <a href="/areacentral/associado?page={{ page.next }}" title="Próxima Página">›</a>
                <a href="/areacentral/associado?page={{ page.last }}" title="Última Página">»</a>
            </p>
        </div>        
    </div>
</div>