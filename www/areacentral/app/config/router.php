<?php

$router = $di->getRouter();

// Define your routes here
$router->add(
  '/associado',
  [
      'controller' => 'associado',
      'action'     => 'lista',
  ]
);

$router->add(
  '/associado/',
  [
      'controller' => 'associado',
      'action'     => 'lista',
  ]
);

$router->handle();
