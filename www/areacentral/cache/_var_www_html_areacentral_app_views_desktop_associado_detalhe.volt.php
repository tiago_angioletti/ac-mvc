<h1 class="title-page">Associados
    <br>
    <small>
        <a href="<?= $this->url->get($this->dispatcher->getControllerName() . '/') ?>"><?= $this->dispatcher->getControllerName() ?></a>
        / <?= $this->dispatcher->getActionName() ?>
    </small>
</h1>

<div class="card">
  <div class="card-header">
    <h2>Ficha do Associado</h2>
  </div>
  <div class="card-body cadastro-area">
    <fieldset class="coluna">
    <h3>Informações Cadastrais</h3>
    <div class="linha">
      <label>Matriz:</label><span class="edit "> <?= $matriz->NOME ?></span>
    </div>
  
    <div class="linha">
      <label>Código:</label><span class="edit size5"><?= $associado->CODIGO ?></span>
    </div>
  
    <div class="linha">
      <label>CNPJ:</label><span class="edit size35"><?= $associado->CNPJ ?></span>
    </div>
  
    <div class="linha">
      <label>Razão Social:</label><span class="edit "><?= $associado->RAZAOSOCIAL ?></span>
    </div>
  
    <div class="linha">
      <label>Nome Fantasia:</label><span class="edit "><?= $associado->NOME ?></span>
    </div>
  
    <div class="linha">
      <label>Insc. Estadual:</label><span class="edit "><?= $associado->IE ?></span>
    </div>
  
    <div class="linha">
      <label>Data de Fundação:</label><span class="edit size20"><?= $associado->DATAFUNDACAO ?></span>
    </div>
  
    <div class="linha">
      <label>Data de Adesão:</label><span class="edit size20"><?= $associado->DATAADESAO ?></span>
    </div>
  
    <div class="linha">
      <label>Data de Adequação:</label><span class="edit size20"><?= $associado->DATAADEQUACAO ?></span>
    </div>
  
    <div class="linha">
      <label>Regime:</label><span class="edit "><?= $associado->REGIME ?></span>
    </div>
  
    <div class="linha">
      <label>Grupos:</label><span class="edit "><?= $associado->GRUPO ?></span>
    </div>
  
    <div class="linha">
      <label>Percentual de Participação nas Metas:</label><span class="edit"><?= $associado->PERCENTUALMETAS ?></span>
    </div>
  
    <div class="linha">
      <label>Percentual de Participação nas Compras:</label><span class="edit"><?= $associado->PERCENTUALCOMPRAS ?></span>
    </div>
  
    <div class="linha">
      <label>Capital Social:</label><span class="edit"><?= $associado->CAPITALSOCIAL ?></span>
    </div>
  <h3>Endereço</h3>
    <div class="linha">
      <label>Endereço:</label><span class="edit "><?= $associado->LOGRADOURO ?>, <?= $associado->BAIRRO ?>, <?= $associado->CEP ?> <br> <?= $associado->CIDADE ?> - <?= $associado->UF ?></span>
    </div>
      </fieldset>

    <fieldset class="coluna">
      <h3>Contato</h3>
        <div class="linha">
          <label>Responsável:</label><span class="edit tags">VERIFICAR</span>
        </div>
      
        <div class="linha">
          <label>Telefone:</label><span class="edit tags"><?= $associado->TELEFONE ?></span>
        </div>
      
        <div class="linha">
          <label>E-mail:</label>
          <span class="edit ">
            <?= $associado->EMAIL ?>
            <a href="mailto:loja1@supercop.com.br">loja1@supercop.com.br</a>                
          </span>
        </div>
      
        <div class="linha">
          <label>Site:</label>
          <span class="edit"><?= $associado->HOMEPAGE ?></span>
        </div>
      
        <div class="linha">
          <label><?= $associado->COMUNICADORTIPO ?></label>
          <span class="edit"><?= $associado->COMUNICADOR ?></span>
        </div>
      <h3>Informações Bancárias</h3>
        <div class="linha">
          <label>Informações Bancárias:</label>
          <span class="edit"><?= $associado->BANCO ?></span>
        </div>
      
        <div class="linha">
          <label>E-mail Financeiro:</label>
          <span class="edit "><?= $associado->EMAILFINANCEIRO ?></span>
        </div>
      <h3>Observações Diversas</h3>
        <div class="linha">
          <label>Observações:</label>
          <span class="edit">
              <?= $associado->OBSERVACAO ?>
          </span>
        </div>
      
          <div class="linha" style="display: none;">
            <label>Logomarca:</label>
            <ul class="fotos miniatura logoupload" style="float:left;">
              <li>
                <a href="#" class="tooltip" id="miniatura"></a>
              </li>
            </ul>
          </div>
    </fieldset>
  </div>              
  <div class="card-footer text-center menu-botoes">
    <a class="btn btn-default" href="?pg=administrador_mensagem_nova&amp;associado=2" title="Enviar Mensagem"><i class="fa fa-send" aria-hidden="true"></i>Enviar Mensagem</a>
    <a class="btn btn-default" href="?pg=administrador_agenda_nova&amp;associado=2" title="Agendar Compromisso"><i class="fa fa-calendar" aria-hidden="true"></i>Agendar Compromisso</a>
    <a class="btn btn-default" href="?pg=administrador_associado_analise_desempenho&amp;associado=2" title="Análise de Desempenho do Associado"><i class="fa fa-bar-chart" aria-hidden="true"></i>Análise de Desempenho</a> 
    <a href="?pg=administrador_associado_alterar&amp;associado=2" class="btn btn-default" title="Alterar Associado"><i class="fa fa-pencil" aria-hidden="true"></i>Alterar</a> 
    <a href="?pg=administrador_associados_lixeira&amp;acao=excluir_associado&amp;associado=2" class="btn btn-default" title="Excluir Associado"><i class="fa fa-trash-o color-red" aria-hidden="true"></i>Excluir</a>
  </div>
</div>

<div class="card">
  <div class="card-header">
    <h2>Contatos e Usuários</h2>
  </div>
  <div class="card-body"></div>
</div>

<div class="card">
  <div class="card-header">
    <h2>Últimas Ocorrências</h2>
  </div>
  <div class="card-body"></div>
</div>

<div class="card">
  <div class="card-header">
    <h2>Arquivos do Associado</h2>
  </div>
  <div class="card-body"></div>
</div>