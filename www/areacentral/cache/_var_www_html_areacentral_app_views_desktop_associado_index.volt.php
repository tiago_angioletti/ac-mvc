<h1 class="title-page">Associados
        <br>
        <small>
            <a href="<?= $this->url->get($this->dispatcher->getModuleName() . '/' . $this->dispatcher->getControllerName() . '/') ?>"><?= $this->dispatcher->getControllerName() ?></a>
            / <?= $this->dispatcher->getActionName() ?>
        </small>
</h1>

<div class="card">
    <form name="frm_filtrar" action="" method="post">
        <input type="hidden" name="filtrar" value="Ok">
        <input type="hidden" name="retirar_filtro" value="">
        <div class="card-header">
            <h2>Filtrar</h2>
        </div>
        <div class="card-body form-inline">
            <div class="card-row">
                <div class="form-group">
                    <select class="form-control" name="filtro_opcoes" id="filtro_opcoes" title="Coluna para Filtrar">
                        <option value="NOME">Nome Fantasia</option>
                        <option value="CNPJ">CNPJ</option>
                        <option value="CODIGO">Código</option>
                        <option value="RAZAOSOCIAL">Razão Social</option>
                        <option value="TELEFONE">Telefone</option>
                        <option value="MARCADOR">Marcador (Associado)</option>
                        <option value="USUARIO">Usuário</option>
                        <option value="UF">UF</option>
                    </select>
                </div>
               
                <div class="form-group">
                    <input class="form-control" name="filtro_valor" id="filtro_valor" title="Valor para Filtrar" type="text" size="30" value="">
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-success" value="Filtrar">
                </div>

                <div class="form-group pull-right">
                    <select class="form-control" name="itens_por_pagina" title="Itens por página" onchange="jRetirar_Filtro(frm_filtrar,''); return false;">
                        <option value="10">10 por página</option>
                        <option value="25">25 por página</option>
                        <option value="50" selected="">50 por página</option>
                        <option value="100">100 por página</option>
                        <option value="200">200 por página</option>
                        <option value="500">500 por página</option>
                        <option value="1000">1000 por página</option>
                        <option value="2000">2000 por página</option>
                    </select>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="card">
    <div class="card-header">
        <h2>Lista de associados</h2>
    </div>
    <div class="card-body form-inline">
        <table class="grid">
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>Cód</th>
                    <th>CNPJ</th>
                    <th>Nome Fantasia</th>
                    <th>Razão Social</th>
                    <th>Telefone(s)</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($associados as $associado) { ?>
                <tr>
                    <td class="centralizado">
                        <input name="selecionar_grupo[]" type="checkbox">
                    </td>
                    <td><?= $associado->CODIGO ?></td>
                    <td><?= $associado->CNPJ ?></td>
                    <td><?= $this->tag->linkTo(['associado/detalhe/' . $associado->CODIGO, $associado->NOME]) ?></td>
                    <td><?= $associado->RAZAOSOCIAL ?></td>
                    <td><?= $associado->TELEFONE ?></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>