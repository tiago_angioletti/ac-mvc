FROM php:7.2-apache

ARG PHALCON_VERSION=3.4.0
ARG PHALCON_EXT_PATH=php7/64bits
ARG INSTALL_VERSION=3.2.13

RUN set -xe && \
    # Install PHP PDO MySql
    docker-php-ext-install pdo pdo_mysql && \    
    # Copia da config do apache rewrite module
    cp /etc/apache2/mods-available/rewrite.load /etc/apache2/mods-enabled/rewrite.load && \
    # Copia da config do apache header module
    cp /etc/apache2/mods-available/headers.load /etc/apache2/mods-enabled/headers.load && \
    # Compile Phalcon
    curl -LO https://github.com/phalcon/cphalcon/archive/v${PHALCON_VERSION}.tar.gz && \
    tar xzf ${PWD}/v${PHALCON_VERSION}.tar.gz && \
    docker-php-ext-install -j $(getconf _NPROCESSORS_ONLN) ${PWD}/cphalcon-${PHALCON_VERSION}/build/${PHALCON_EXT_PATH} && \
    # Istall Phalcon Dev Tools
    curl -LO https://github.com/phalcon/phalcon-devtools/archive/v${INSTALL_VERSION}.tar.gz && \    
    tar xzf v${INSTALL_VERSION}.tar.gz && \
    rm -rf v${INSTALL_VERSION}.tar.gz && \
    mv phalcon-devtools-${INSTALL_VERSION} /usr/src/phalcon-devtools && \
    ln -sf /usr/src/phalcon-devtools/phalcon.php /usr/local/bin/phalcon && \
    # Remove all temp files
    rm -r \
        ${PWD}/v${PHALCON_VERSION}.tar.gz \
        ${PWD}/cphalcon-${PHALCON_VERSION}